# SQL test


How many copies of 	the book titled The Lost Tribe are owned by the library branch whose name is "Sharpstown"?
```sql
select
copies.no_of_copies
from book_copies copies
where 
copies.bookid = (
    select 
    bookid 
    from 
    book 
    where title = 'The Lost Tribe')
and 
copies.branchid = (
    select 
    branchid 
    from 
    library_branch 
    where 
    branchname = 'Sharpstown');
``` 

----

How many copies of 	the book titled The Lost Tribe are owned by each library branch?

```sql
select 
library.branchname as branch,
copies.no_of_copies as number_of_copies
from 
LIBRARY_BRANCH library,
BOOK_COPIES copies
where
copies.bookid = (
    select 
    book.bookid 
    from 
    BOOK book 
    where 
    book.title = 'The Lost Tribe')
and
library.branchid = copies.branchid;
```

----

Retrieve the names 	of all borrowers who do not have any books checked out .

```sql
select 
borrower.name as borrower_name
from 
BORROWER borrower
LEFT JOIN 
BOOK_LOANS loans 
ON
loans.cardno = borrower.cardno
where
loans.bookid IS NULL
```

-----


For each book that 	is loaned out from the "Sharpstown" branch and whose 	DueDate is today, retrieve the book title, the borrower's name, and 	the borrower's address.

```sql
select 
book.title as book_title,
borrower.name as borrower_name,
borrower.address as borrower_address
from 
BOOK_LOANS loans,
BORROWER borrower,
BOOK book
where
loans.cardno = borrower.cardno
and
loans.bookid = book.bookid
and
loans.branchid = (
    select 
    library.branchid 
    from 
    LIBRARY_BRANCH 
    library 
    where 
    library.branchname = 'Sharpstown')
and loans.duedate = curdate();

```
For each library branch, retrieve the branch name and the total number of books loaned out from that branch.
```sql
select 
    (select 
    branchname 
    from 
    library_branch 
    where branchid = loans.branchid ) as branch_name,
count(1) as number_of_books
from 
BOOK_LOANS loans 
group by 
loans.branchid;
```

-----

Retrieve the names, addresses, and number of books checked out for all borrowers who have more than five books checked out.

```sql
select bor.name as name,
bor.address as address,
count(1) as number_ofloans 
from 
BOOK_LOANS loans,
borrower bor
where bor.cardno = loans.cardno
group by 
loans.cardno 
HAVING COUNT(1)>5;
```
-----

For each book authored (or co-authored) by "Stephen King", retrieve the title and the number of copies owned by the library branch whose name is "Central"

```sql
select
(select title from book where bookid = copies.bookid ),
copies.no_of_copies
from
book_copies copies
where
copies.bookid in (
  select  
  authors.bookid 
  from 
  book_authors authors
  where 
  authors.authorname = 'Stephen King')
  and copies.branchid = (
    select 
    branch.branchid 
    from 
    library_branch branch 
    where 
    branch.branchname = 'Central');
```
