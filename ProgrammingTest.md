# Programing testing
# 3 devisors

``` java

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

public class Divisors {

    static int numbersWith3Divisors(int a, int b) {
        if (a >= b) {
            System.out.println("First parameter 'a' should be less then 'b'. \n Please try again with correct input");
            System.exit(1);
        }
        if (a > 999999999 || a < 0 ) {
            System.out.println("Parameters 'a' should be greater then 0 and less or equal to 999999999. \n Please try again with correct input");
            System.exit(1);
        }
        if (b > 999999999 || b < 0) {
            System.out.println("Parameters 'b' should be greater then 0 and less or equal to 999999999. \n Please try again with correct input");
            System.exit(1);
        }
        boolean[] prime = new boolean[b + 1];
        Arrays.fill(prime, true);
        prime[0] = prime[1] = false;

        for (int p = 2; p * p <= b; p++) {
            if (prime[p]) {
                for (int i = p * 2; i <= b; i += p)
                    prime[i] = false;
            }
        }
        int count = 0;
        for (int i = 0; i * i <= b; i++)
            if (prime[i] && i * i > a) {
                count++;
            }
        return count;
    }

    public static void main(String[] args) {
        Instant start = Instant.now();
        int numbersWith3Divisors = numbersWith3Divisors(11, 50);
        Instant finish = Instant.now();
        System.out.println("==========================");
        System.out.println("number of dividers: " + numbersWith3Divisors);
        System.out.println("==========================");
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println("Time: " + timeElapsed + " millis");
    }
}

```
